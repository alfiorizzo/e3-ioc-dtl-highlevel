# DTL ORCHESTRATION Application (High Level CS)
# developed by Maurizio Montis | INFN-LNL
#
# the application provides the main state machine devoted to manage
#   the entire DTL apparatus.
# controls and logic are based on the documentation "DTL Concept of Operations"
#


require essioc
require dtlhighlevel 

# Load Common EPICS Modules
iocshLoad("$(essioc_DIR)/common_config.iocsh")

## SQL State Machines
# Orchestration - State Machine 
iocshLoad("$(dtlhighlevel_DIR)dtl-statemachine-pv.iocsh")
iocshLoad("$(dtlhighlevel_DIR)postInitStateMachine-cfg.iocsh")


